PRAGMA foreign_keys=OFF;

DROP TABLE IF EXISTS cookies;
DROP TABLE IF EXISTS pallets;
DROP TABLE IF EXISTS orders;
DROP TABLE IF EXISTS order_size;
DROP TABLE IF EXISTS customers;
DROP TABLE IF EXISTS materials;
DROP TABLE IF EXISTS ingredients;

PRAGMA foreign_keys=ON;

CREATE TABLE cookies (
    cookie_name     TEXT,
    PRIMARY KEY     (cookie_name)
);

CREATE TABLE pallets (
    pallet_id       TEXT,
    cookie_name     TEXT,
    prod_date       DATETIME DEFAULT current_timestamp,
    location        TEXT,
    delivery_date   DATE,
    delivery_time   TIME,
    order_id        TEXT,
    blocked         BOOLEAN,
    PRIMARY KEY     (pallet_id),
    FOREIGN KEY     (cookie_name) REFERENCES cookies(cookie_name),
    FOREIGN KEY     (order_id) REFERENCES orders(order_id)
);

CREATE TABLE orders (
    order_id        TEXT,
    delivery_time   TIME,
    customer_id     TEXT,
    PRIMARY KEY     (order_id),
    FOREIGN KEY     (customer_id) REFERENCES customers(customer_id)
);

CREATE TABLE order_size (
    pallet_amount   INT CHECK(pallet_amount >= 1),
    cookie_name     TEXT,
    order_id        TEXT,
    FOREIGN KEY     (cookie_name) REFERENCES cookies(cookie_name),
    FOREIGN KEY     (order_id) REFERENCES orders(order_id)
);

CREATE TABLE customers (
    customer_id     TEXT,
    customer_name   TEXT,
    address         TEXT,
    PRIMARY KEY     (customer_id)
);

CREATE TABLE materials (
    material_name   TEXT,
    material_amount FLOAT DEFAULT 0,
    unit            TEXT,
    delivery_date   DATE,
    delivery_amount INT,
    PRIMARY KEY     (material_name)
);

CREATE TABLE ingredients (
    material_name       TEXT,
    ingredient_amount   FLOAT,
    cookie_name         TEXT,
    PRIMARY KEY (material_name, cookie_name),
    FOREIGN KEY (cookie_name) REFERENCES cookies(cookie_name),
    FOREIGN KEY (material_name) REFERENCES materials(material_name)
);

CREATE TRIGGER enough_ingredients
    AFTER UPDATE ON materials
    BEGIN
    SELECT
    CASE
    WHEN new.material_amount < 0
    THEN
    RAISE (ROLLBACK, "Not enough ingredients in storage")
END;
END;
