#!/usr/bin/env python
# -*- coding: utf-8 -*-


from bottle import get, post, run, response, request, route
from datetime import datetime, timedelta
import sqlite3
import json
from urllib.parse import unquote, quote



conn = sqlite3.connect("cookies.db")
conn.execute('Pragma foreign_keys = true')

def format_response(d):
    return json.dumps(d, indent=4) + "\n"


@post('/reset')
def post_reset():
   response.content_type = 'application/json'
   c = conn.cursor()
   file = open("create-schema.sql","r")
   string = file.read()
   c.executescript(string)
   conn.commit()
   response.status = 205
   return {"status":"ok"}
         



    

@post('/customers')
def  post_customers():
    response.content_type = 'application/json'
    customer =  request.json
    c = conn.cursor()
    try: 
        c.execute(
            """
            INSERT 
            INTO customers(customer_name, address)
            VALUES (?,?)
            """, 
            [customer['name'], customer['address']]
        )
        conn.commit()
        response.status = 201
        url_encode_name = quote(customer['name'])
        return {"location": f"/customers/{url_encode_name}"}
    except sqlite3.IntegrityError:
        response.status = 400
        return "Error"

@get('/customers')
def get_customers():
    c = conn.cursor()
    #response.content_type = 'application/json'
    c.execute(
        """
        SELECT customer_name, address
        FROM   customers
        """
    )
    found = [{"name": customer_name, "address": address}
    for customer_name, address in c]
    response.status = 200
    return {"data":found}


@post('/ingredients')
def post_ingredients():
    response.content_type = 'application/json'
    ingredients  = request.json
    c = conn.cursor()
    try:
        c.execute(
            """
            INSERT 
            INTO materials(material_name, unit)
            VAlUES (?,?)
            """,
            [ingredients['ingredient'], ingredients['unit']]
        )
        print("geh")
        conn.commit()
        response.status = 201
        url_encode_ingredient = quote(ingredients['ingredient'])
        return {"location": f"/ingredients/{url_encode_ingredient}"}
    
    except sqlite3.IntegrityError:
        response.status = 400
        return "Error"

@post(f"/ingredients/<ingredient_name>/deliveries")
def post_deliveries(ingredient_name):
    delivery = request.json
    c = conn.cursor()
    try: 
        c.execute(
            """
            UPDATE materials
            SET delivery_date = ?, delivery_amount = ?, material_amount =  material_amount + ?
            WHERE material_name = ? 
            """,
            [delivery['deliveryTime'], delivery['quantity'], delivery['quantity'], ingredient_name]
        )
        conn.commit()
        found = [{"ingredient": ingredient_name, "quantity": material_amount, "unit": unit}
        for material_amount, unit in c]
        response.status = 201
        return {"data": found}

    except sqlite3.IntegrityError:
        response.status = 400
        return "Error"




@get('/ingredients')
def get_ingredients():
    c = conn.cursor()
    c.execute(
        """
        SELECT material_name, material_amount, unit
        FROM materials
        """
    )
    conn.commit()
    found = [{"ingredient": ingredient_name, "quantity": material_amount, "unit": unit}
    for ingredient_name, material_amount, unit in c]

    response.status = 200
    return {"data":found}

@post('/cookies')
def post_cookies():
    cookies = request.json
    c = conn.cursor()

    try: 
        c.execute(
            """
            INSERT
            INTO cookies(cookie_name)
            VALUES (?)
            """,
            [cookies['name']]
        )
        conn.commit()
        for ingredient in cookies["recipe"]:
            c.execute(
                """
                INSERT
                INTO ingredients(ingredient_amount, cookie_name, material_name)
                VALUES(?,?,?)
                """,
                [ingredient['amount'], cookies['name'], ingredient['ingredient']]
            )
            conn.commit()

        response.status = 201
        url_encode_name = quote(cookies['name'])
        return {"location": f"/cookies/{url_encode_name}"}

    except sqlite3.IntegrityError:
        response.status = 400
        return "Error"

@get('/cookies')
def get_cookies():
    c  = conn.cursor()

    c.execute(
        """
        SELECT cookie_name, COALESCE(count(pallet_id) - sum(blocked),0) as blocked_pallets
        FROM cookies
        LEFT JOIN pallets USING (cookie_name)
		GROUP BY cookie_name
        """
    )
    conn.commit()
    found = [{"name": cookie_name, "pallets": blocked_pallets}
    for cookie_name, blocked_pallets in c]

    response.status = 200
    return {"data":found}

@get('/cookies/<cookie_name>/recipe')
def get_cookie_recipe(cookie_name):
    c = conn.cursor()
    try:
        c.execute(
            """
            SELECT material_name, ingredient_amount, unit 
            FROM materials 
            JOIN ingrediens
            USING (material_name)
            WHERE cookie_name = ?
            """,
            [cookie_name]
        )
        found = [{"ingredient": material_name, "amount": ingredient_amount, "unit": unit}
        for material_name, ingredient_amount, unit in c]
        response.status = 200
        return{"data": found}



    except sqlite3.IntegrityError:
        response.status = 404
        return {"data":found}

@post('/pallets')
def post_pallets():
    pallet = request.json
    c = conn.cursor()
    c.execute(
        """
        SELECT material_name, ingredient_amount
        FROM ingredients
        WHERE cookie_name = ?
        """
        ,
        [pallet["cookie"]]
    )
    cookies_per_pallet = 5400
    cookies_per_recipe = 100
    found = [{"ingredient": material_name, "recipes_amount": -ingredient_amount*cookies_per_pallet/cookies_per_recipe}
                for material_name, ingredient_amount in c]
    print(found)
    for ingredient in found:
        try:
            c.execute(
                """
                UPDATE materials
                SET material_amount = material_amount + ?
                WHERE material_name = ?
                
                """,
                [ingredient['recipes_amount'], ingredient['ingredient']]
            )
        except:
            response.status = 422
            return {"location":""}
    c.execute(
        """
        INSERT
        INTO pallets(cookie_name)
        VALUES (?)
        """,
        [pallet["cookie"]]
    )
    c.execute(
        """
        SELECT pallet_id
        FROM pallets
        WHERE rowid = last_insert_rowid()
        """
    )
    response.status = 201
    conn.commit()
    pallet_id = c.fetchone()[0]
    return {"location": f"/pallets/{pallet_id}"}

@get('/pallets')
def get_pallets():
    
    after = request.query.get("after")
    before = request.query.get("before")
    cookie = request.query.get("cookie")

    start_date = "0000-01-01"
    end_date = "9999-12-31"

    if(after != None):
        start_date = datetime.strptime(after, '%Y-%m-%d') + timedelta(days=1)
    if(before != None):
        end_date =  datetime.strptime(before, '%Y-%m-%d')
    c = conn.cursor()
    if(cookie != None):
        c.execute(
            """
            SELECT pallet_id, cookie_name, prod_date, blocked
            FROM pallets
            WHERE cookie_name = ? AND prod_date BETWEEN ? AND ?
            """, 
            [cookie, start_date, end_date]
        )
    else:
        c.execute(
            """
            SELECT pallet_id, cookie_name, prod_date, blocked
            FROM pallets
            WHERE prod_date BETWEEN ? AND ?
            """, 
            [start_date, end_date]
        )
    found = [{"id": pallet_id, "cookie": cookie_name, "productionDate": prod_date, "blocked": blocked}
                for pallet_id, cookie_name, prod_date, blocked in c]

    response.status = 200
    return {"data":found}

@post('/cookies/<cookie_name>/block')
def block_cookie(cookie_name):
    after = request.query.get("after")
    before = request.query.get("before")
    start_date = "0000-01-01"
    end_date = "9999-12-31"
    
    if(after != None):
        start_date = datetime.strptime(after, '%Y-%m-%d') + timedelta(days=1)
    if(before != None):
        end_date =  datetime.strptime(before, '%Y-%m-%d')
    c = conn.cursor()
    c.execute( 
        """
        UPDATE pallets
        SET blocked = 1
        WHERE cookie_name = ? AND prod_date BETWEEN ? AND ?
        """, 
        [cookie_name, start_date, end_date]
    )
    response.status = 205
    conn.commit()
    return {"data": ""}

@post('/cookies/<cookie_name>/unblock')
def unblock_cookie(cookie_name):
    after = request.query.get("after")
    before = request.query.get("before")
    start_date = "0000-01-01"
    end_date = "9999-12-31"
    
    if(after != None):
        start_date = datetime.strptime(after, '%Y-%m-%d') + timedelta(days=1)
    if(before != None):
        end_date =  datetime.strptime(before, '%Y-%m-%d')
    c = conn.cursor()
    c.execute( 
        """
        UPDATE pallets
        SET blocked = 0
        WHERE cookie_name = ? AND prod_time BETWEEN ? AND ?
        """, 
        [cookie_name, start_date, end_date]
    )
    response.status = 205
    conn.commit()
    return {"data": ""}


run(host='localhost', port=8888)