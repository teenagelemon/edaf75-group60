# EDAF75, project report

This is the report for

 + David Ekblad, `da6135ek-s`
 + Moa Samuelsson `mo0353sa-s`

We solved this project on our own, except for:

 + The Peer-review meeting



## ER-design

The model is in the file [`er-model.png`](er-model.png):

<center>
    <img src="er-model.png">
</center>




## Tables

The ER-model above gives the following tables (including keys and
foreign keys):

```text
table pallets:
  pallet_id
  cookie_name
  prod_date
  location
  delivery_date
  delivery_time
  order_id
  blocked
  PK: pallet_id
  FK: cookie_name -> cookies(cookie_name)
  FK: order_id -> orders(order_id)

table cookies:
  cookie_name
  PK: cookie_name

table orders:
  order_id
  delivery_time
  customer_id
  PK: order_id
  FK: customer_id -> customers(customer_id)

table order_size:
  pallet_amount
  cookie_name
  order_id
  PK: cookie_name
  FK: order_id -> orders(order_id)

table customers:
  customer_id
  customer_name
  address
  PK: customer_id

table materials:
  material_name
  material_amount
  unit
  delivery_date
  delivery_amount
  PK: material_name

table ingredients:
  material_name
  ingredient_amount
  cookie_name
  PK: material_name, cookie_name
  FK: (cookie_name) -> cookies(cookie_name)
  FK: material_name -> materials(material_name)
```



## Scripts to set up database

The scripts used to set up and populate the database are in:

 + [`create-schema.sql`](create-schema.sql) (defines the tables)

So, to create and initialize the database, we run:

```shell
sqlite3 krusty-db.sqlite < create-schema.sql

```



## How to compile and run the program

This section should give a few simple commands to type to
compile and run the program from the command line, such as:

```shell
python api.py
```


